# xsDoge | PKQ 信息更新
本项目用于发布最新信息, 请收藏网页或星标

请同时**收藏**下列网址 **作为备份**:

https://github.com/xsDoge/info


## 信息更新

### 域名
最新域名:
https://www.pkqcloudx.com

永久域名:

https://xsdoge.com

https://pkqcloud.com

https://xSpeedyDoge.com

#### 以上方法均失效时, 可联系管理员邮箱
邮箱:
dogecare@protonmail.com

---
---
[20250129更新]
## 公告 | Announcement

#### [通知: 新年活动]
### **庆祝 乙巳蛇年新年 全站季度付 半年付 年付85折**

#### ⚠️购买前请认真阅读下面几点说明！

#### ⚠️套餐无法叠加，如果当前账户有套餐，一定要先折算现有套餐再用优惠码买新套餐！
#### ⚠️如已买了新套餐，才发现之前老套餐没折算，直接找客服帮忙处理，请勿自行折算！

折算流程：
1. 点击 https://www.pkqcloudx.com/user/code 进入钱包界面
2. 下滑到底部 `套餐记录` ，点击现有套餐旁的齿轮可将现套餐折算为余额

购买时请输入:
`HappyNewYearbsIIYgoj`
额度：15% off; 季度付(含)以上85折
有效期至: 2025-02-19 23:59 PM (UTC+8)
每位用户活动期间仅限使用一次!
